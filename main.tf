module "lxc" {
    source = "./modules/lxc"
    node_name = "wcs-cyber-node01"
    pve_api_token = ""
    pve_ssh_user = "root"
    tmp_dir = "/tmp"


    ##container
    ct_id = "201"
    ct_network_name "ethe"
    ct_network_bridge
    "vmbr2"
    ct_datastore_storage_location = "local-nvme-datas" ct_datastore_template_location = "local-hdd-templates"
    ct_disk_size
    8
    ct_memory = 1024
    ct_source_file_path = "local-hdd-templates:vzmtpl/debian-12-standard
    ct_hostname = "tofu-poof"
    dns_domain= "tofu"
    dns_servers = []
    gateway = "192.168.1.254"
    os_type "debian"
}