terraform {
  required_providers {
    proxmox = {
      source  = "bpg/proxmox"
      version = ">= 0.38.1"
    }
    random = {
      source = "hashicorp/random"
    }
  }
}
provider "proxmox" {
  endpoint  = var.venv_endpoint
  api_token = var.venv_api_token
  insecure  = true
  ssh {
    agent    = true
    username = var.ssh_username
  }
  tmp_dir = var.tmp_dir
}