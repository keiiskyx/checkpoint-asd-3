variable "venv_endpoint" {
  description = "endpoint"
  type        = string
  default     = ""
}

variable "venv_api_token" {
  description = "Le token de l'api pm"
  type        = string
  default     = ""
}


variable "venv_username" {
  description = "Le username virtuel"
  type        = string
  default     = ""
}

variable "venv_password" {
  description = "Le password "
  type        = string
  default     = ""
}

variable "ssh_username" {
  description = "Le username "
  type        = string
  default     = ""
}

variable "venv_filepath" {
  description = "Le chemin clé rsa"
  type        = string
  default     = ""
}

variable "tmp_dir" {
  description = "Le dossier temp"
  type        = string
  default     = ""
}